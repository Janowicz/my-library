package com.library;

import java.util.*;

public class RepositoryReservation {

    private Map<ISBN, Status> repositoryReservationMap = new HashMap<>();
    private Set<ISBN> repositoryReservationKeyMap = new HashSet<>();

    public Set<ISBN> getISBNList(){

        repositoryReservationKeyMap = repositoryReservationMap.keySet();
        return repositoryReservationKeyMap;
    }

    public Status checkValueFromISBN(ISBN isbn){

        return repositoryReservationMap.get(isbn);
    }

    public Set<ISBN> getAvailableBooks(){

        Set<ISBN> availableBooks = new HashSet();
        repositoryReservationKeyMap = new HashSet<>();
        for (ISBN status : repositoryReservationMap.keySet()){
            if (repositoryReservationMap.get(status).equals(Status.AVAILABLE)){
                availableBooks.add(status);
            }
        }
        return availableBooks;
    }

    public Set<ISBN> getRentedBooks(){

        Set<ISBN> rentedBooks = new HashSet();
        repositoryReservationKeyMap = new HashSet<>();
        for (ISBN status : repositoryReservationMap.keySet()){
            if (repositoryReservationMap.get(status).equals(Status.RENTED)){
                rentedBooks.add(status);
            }
        }
        return rentedBooks;
    }

    public Collection<Status> getAllValues(){

        Collection<Status> listOfAllValues = repositoryReservationMap.values();
        return listOfAllValues;
    }

    public void returnBookToRepository(ISBN returnISBN) {

        repositoryReservationMap.replace(returnISBN, Status.RENTED, Status.AVAILABLE);
    }

    public void rentBook(ISBN rentISBN) {

        repositoryReservationMap.replace(rentISBN, Status.AVAILABLE, Status.RENTED);
    }


    public void deleteBook(ISBN deleteISBN) {

        repositoryReservationMap.remove(deleteISBN);
    }

    public void putInRepositoryReservationMap(ISBN isbn) {

        repositoryReservationMap.put(isbn, Status.AVAILABLE);
    }
}
