//package com.newLibrary;
//
//import java.io.FileNotFoundException;
//import java.io.IOException;
//import java.util.Collection;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Set;
//
//public class OldCodeToWorkAtMemory {
//
//    public class BookRepository {
//
//        private Map<ISBN, Book> bookRepositoryMap = new HashMap<>();
//        private Set<ISBN> bookRepositoryKeyMap;
//        private Collection<Book> valueMap;
//        private InteractionWithUser interactionWithUser = new InteractionWithUser();
//
//        public void getAllBooks(){
//
//            if (bookRepositoryMap.isEmpty()){
//                System.out.println("Book repository is empty!");
//            } else {
//                bookRepositoryKeyMap = bookRepositoryMap.keySet();
//                System.out.println("List of all books:");
//                for (ISBN key : bookRepositoryKeyMap) {
//                    System.out.println(bookRepositoryMap.get(key));
//                }
//            }
//        }
//
//        public void searchBook() throws IOException {
//
//            if (bookRepositoryMap.isEmpty()){
//                System.out.println("Book repository is empty!");
//            } else {
//                System.out.println("Enter part of text to search a book.");
//
//                String search = interactionWithUser.loadCharactersString();
//
//                valueMap = bookRepositoryMap.values();
//
//                System.out.println("\nResults:");
//                for (Book value : valueMap) {
//                    if (value.getAuthor().contains(search) || value.getTitle().contains(search)
//                            || value.getYear().toString().contains(search) || value.getIsbn().toString().contains(search)) {
//                        System.out.println(value);
//                    }
//                }
//            }
//        }
//
//        public boolean ifBookContainsISBN(ISBN isbn){
//
//            if (bookRepositoryMap.containsKey(isbn)){
//                return true;
//            } else {
//                return false;
//            }
//        }
//
//        public void deleteBook(ISBN isbn){
//
//            if (bookRepositoryMap.containsKey(isbn)){
//                bookRepositoryMap.remove(isbn);
//                System.out.println("The book has been removed");
//            } else {
//                System.out.println("Book with this ISBN doesn't exists!");
//            }
//        }
//
//        public Book returnBookFromISBN(ISBN isbn){
//
//            return bookRepositoryMap.get(isbn);
//
//        }
//
//        public void putToBookRepositoryMap(ISBN isbn, Book book){
//
//            bookRepositoryMap.put(isbn, book);
//
//        }
//
//    }
//
//    public class RepositoryReservation {
//
//        private Integer year = null;
//        protected com.newLibrary.BookRepository bookRepository = new com.newLibrary.BookRepository();
//        private Map<ISBN, Status> repositoryReservationMap = new HashMap<>();
//        private Set<ISBN> bookRepositoryKeyMap;
//        private InteractionWithUser interactionWithUser = new InteractionWithUser();
//
//        public RepositoryReservation() throws FileNotFoundException {
//        }
//
//        public void ifExist() throws IOException {
//
//            ISBN isbn = interactionWithUser.writeISBN();
//
//            bookRepositoryKeyMap = repositoryReservationMap.keySet();
//
//            if (bookRepositoryKeyMap.contains(isbn)) {
//                System.out.println("The book with whis ISBN is avalaible in library repository!");
//            } else {
//                String title = interactionWithUser.writeTitle();
//                String author = interactionWithUser.writeAuthor();
//                Integer year = interactionWithUser.writeYear();
//                Book book = new Book(title, author, year, isbn);
//                repositoryReservationMap.put(isbn, Status.AVAILABLE);
//                bookRepository.putToBookRepositoryMap(isbn, book);
//                System.out.println("Book has been added!");
//            }
//        }
//
//        public void showAvailableBooks() {
//
//            if (repositoryReservationMap.isEmpty()) {
//                System.out.println("Book repository is empty!");
//            } else {
//
//                bookRepositoryKeyMap = repositoryReservationMap.keySet();
//
//                System.out.println("List of all available books:");
//
//                if (repositoryReservationMap.values().contains(Status.AVAILABLE)) {
//                    for (ISBN key : bookRepositoryKeyMap) {
//                        if (repositoryReservationMap.get(key).equals(Status.AVAILABLE)) {
//                            System.out.println("ISBN: " + key + " STATUS: " + repositoryReservationMap.get(key));
//                        }
//                    }
//                } else {
//                    System.out.println("There aren't available books in the repository!");
//                }
//            }
//        }
//
//        public void returnBookToRepository() throws IOException {
//
//            if (repositoryReservationMap.isEmpty()) {
//                System.out.println("Book repository is empty!");
//            } else {
//                System.out.println("List of all rented books:");
//                if (repositoryReservationMap.values().contains(Status.RENTED)) {
//                    for (ISBN key : bookRepositoryKeyMap) {
//                        if (repositoryReservationMap.get(key).equals(Status.RENTED)) {
//                            System.out.println("ISBN: " + key + " STATUS: " + repositoryReservationMap.get(key));
//                        }
//                    }
//
//                    System.out.println("Enter the ISBN of the book which you want to return.");
//                    ISBN returnISBN = interactionWithUser.writeISBN();
//
//                    if (bookRepository.ifBookContainsISBN(returnISBN) && repositoryReservationMap.get(returnISBN).equals(Status.AVAILABLE)) {
//                        System.out.println("This book has been rented earlier!");
//                    } else if (bookRepository.ifBookContainsISBN(returnISBN) && repositoryReservationMap.get(returnISBN).equals(Status.RENTED)) {
//                        Book returnBookToRepository = bookRepository.returnBookFromISBN(returnISBN);
//                        System.out.println("Do you want return the book: " + returnBookToRepository + "? 1 - YES, other integer number - NO.");
//                        String choose = interactionWithUser.loadCharactersString();
//                        Integer chooseInt = interactionWithUser.toInt(choose);
//                        switch (chooseInt) {
//                            case 1:
//                                repositoryReservationMap.replace(returnISBN, Status.RENTED, Status.AVAILABLE);
//                                System.out.println("The book is available now!");
//                                break;
//                            default:
//                                break;
//                        }
//                    } else {
//                        System.out.println("Book with this ISBN doesn't exists!");
//                    }
//
//                } else {
//                    System.out.println("There aren't rented books in the repository!");
//                }
//
//
//            }
//        }
//
//        public void rentBook() throws IOException {
//
//            if (repositoryReservationMap.isEmpty()) {
//                System.out.println("Book repository is empty!");
//            } else {
//                System.out.println("Enter the ISBN of the book which you want to rent.");
//                ISBN rentISBN = interactionWithUser.writeISBN();
//                if (bookRepository.ifBookContainsISBN(rentISBN) && repositoryReservationMap.get(rentISBN).equals(Status.RENTED)) {
//                    System.out.println("This book is now rented!");
//                } else if (bookRepository.ifBookContainsISBN(rentISBN) && repositoryReservationMap.get(rentISBN).equals(Status.AVAILABLE)){
//                    Book rentBook = bookRepository.returnBookFromISBN(rentISBN);
//                    System.out.println("Do you want to rent the book: " + rentBook + "? 1 - YES, other integer number - NO.");
//                    String choose = interactionWithUser.loadCharactersString();
//                    Integer chooseInt = interactionWithUser.toInt(choose);
//                    switch (chooseInt) {
//                        case 1:
//                            repositoryReservationMap.replace(rentISBN, Status.AVAILABLE, Status.RENTED);
//                            System.out.println("The book has been rented!");
//                            break;
//                        default:
//                            break;
//                    }
//                } else {
//                    System.out.println("Book with this ISBN doesn't exists!");
//                }
//            }
//        }
//
//        public void deleteBook() throws IOException {
//
//            if (repositoryReservationMap.isEmpty()) {
//                System.out.println("Book repository is empty!");
//            } else {
//                System.out.println("Enter the ISBN of the book which you want to delete.");
//                ISBN deleteISBN = interactionWithUser.writeISBN();
//
//                bookRepository.deleteBook(deleteISBN);
//                repositoryReservationMap.remove(deleteISBN);
//            }
//        }
//    }
//
//
//}
