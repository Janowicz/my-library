package com.library;

import java.io.*;
import java.util.Optional;

public class MyLibrary {

    private RepositoryReservation repositoryReservation = new RepositoryReservation();
    private BookRepository bookRepository = new BookRepository();
    private BufferedReader reader;

    public void run() throws IOException {

        readFile();

        while (true) {
            System.out.println(
                    " _________________________________________________________\n" +
                            "| Wybierz:                                                |\n" +
                            "| 1 - view catalog of all books                           |\n" +
                            "| 2 - view catalog of available books                     |\n" +
                            "| 3 - rent a book                                         |\n" +
                            "| 4 - return a book                                       |\n" +
                            "| 5 - search a book                                       |\n" +
                            "| 6 - add a book                                          |\n" +
                            "| 7 - remove a book                                       |\n" +
                            "| 'Exit' - exit                                           |\n" +
                            " _________________________________________________________");

            String choose = loadCharactersString();
            mySwitch(choose);
        }
    }

    public void mySwitch(String choose) throws IOException {

        switch (choose) {
            case "Exit":
                exit();
                break;
            case "1":
                if (bookRepository.checkIfEmptyRepository()) {
                    System.out.println("Book repository is empty!");
                } else {
                    System.out.println("List of all books:");
                    for (Book collection : bookRepository.getAllBooks()) {
                        System.out.println(collection);
                    }
                }
                break;
            case "2":
                if (repositoryReservation.getISBNList().isEmpty()) {
                    System.out.println("Book repository is empty!");
                } else {
                    if (!repositoryReservation.getAllValues().contains(Status.AVAILABLE)) {
                        System.out.println("There are not available books to rent!");
                    } else {
                        System.out.println("List of all available books:");
                        for (ISBN availableBook : repositoryReservation.getAvailableBooks()) {
                            System.out.println(bookRepository.returnBookFromISBN(availableBook));
                        }
                    }
                }
                break;
            case "3":
                if (!repositoryReservation.getISBNList().isEmpty()) {
                    System.out.println("List of all available books:");
                    for (ISBN availableBook : repositoryReservation.getAvailableBooks()) {
                        System.out.println(bookRepository.returnBookFromISBN(availableBook));
                    }
                    ISBN rentISBN = writeISBN();
                    Optional<String> book = bookRepository.returnBookFromISBN(rentISBN)
                            .map(Book::toString);

                    if (repositoryReservation.getISBNList().contains(rentISBN) &&
                            repositoryReservation.checkValueFromISBN(rentISBN).equals(Status.AVAILABLE)) {
                        repositoryReservation.rentBook(rentISBN);
                        System.out.println("Book " + book + " has been rented!");
                    } else if ((repositoryReservation.getISBNList().contains(rentISBN) &&
                            repositoryReservation.checkValueFromISBN(rentISBN).equals(Status.RENTED))) {
                        System.out.println("This book has been rented earlier!");
                    } else {
                        System.out.println("Book with this ISBN doesn't exists!");
                    }

                } else {
                    System.out.println("Book repository is empty!");
                }
                break;
            case "4":
                if (!repositoryReservation.getISBNList().isEmpty()) {
                    if (repositoryReservation.getAllValues().contains(Status.RENTED)) {
                        System.out.println("List of rented books:");
                        for (ISBN rentedBooks : repositoryReservation.getRentedBooks()){
                            System.out.println(bookRepository.returnBookFromISBN(rentedBooks));
                        }
                        ISBN returnISBN = writeISBN();
                        if (repositoryReservation.getISBNList().contains(returnISBN)
                                && repositoryReservation.checkValueFromISBN(returnISBN).equals(Status.AVAILABLE)) {
                            System.out.println("The book was available earlier!");
                        } else if (repositoryReservation.getISBNList().contains(returnISBN)
                                && repositoryReservation.checkValueFromISBN(returnISBN).equals(Status.RENTED)) {
                            repositoryReservation.returnBookToRepository(returnISBN);
                            System.out.println("The book is available now!");
                        } else {
                            System.out.println("Book with this ISBN doesn't exists!");
                        }
                    }
                } else {
                    System.out.println("Book repository is empty!");
                }
                break;
            case "5":
                boolean isEmptyBookRepository = bookRepository.checkIfEmptyRepository();
                if (!isEmptyBookRepository) {
                    System.out.println("Enter part of text to search a book.");
                    String search = loadCharactersString();

                    System.out.println("\nResults:");
                    for (Book value : bookRepository.getAllBooks()) {
                        if (value.getAuthor().contains(search) || value.getTitle().contains(search)
                                || value.getYear().toString().contains(search) || value.getIsbn().toString().contains(search)) {
                            System.out.println(value);
                        }
                    }
                } else {
                    System.out.println("Book Repository is empty!");
                }
                break;
            case "6":
                ISBN isbn = writeISBN();
                if (!repositoryReservation.getISBNList().contains(isbn)) {
                    String title = writeTitle();
                    String author = writeAuthor();
                    Integer year = writeYear();
                    if (year < 0) {
                        System.out.println("Incorrect year!");
                    } else {
                        Book book = new Book(title, author, year, isbn);
                        System.out.println("Do you want to add book: " + book + "? \nSelect 1 - YES, other button - NO.");
                        String one = loadCharactersString();
                        switch (one) {
                            case "1":
                                repositoryReservation.putInRepositoryReservationMap(isbn);
                                bookRepository.putToBookRepositoryMap(isbn, book);
                                System.out.println("The book has been added!");
                                break;
                            default:
                                System.out.println("The book has not been added!");
                                break;
                        }
                    }
                } else {
                    System.out.println("The book with this ISBN is avalaible in library repository!");
                }
                break;
            case "7":
                if (!repositoryReservation.getISBNList().isEmpty()) {
                    System.out.println("Enter the ISBN of the book which you want to delete.");
                    ISBN deleteISBN = writeISBN();
                    if (bookRepository.ifBookContainsISBN(deleteISBN)) {
                        Optional<Book> book = bookRepository.returnBookFromISBN(deleteISBN);
                        System.out.println("Do you want to delete " + book + "? Select 1 - YES, other button - NO.");
                        String one = loadCharactersString();
                        switch (one) {
                            case "1":
                                repositoryReservation.deleteBook(deleteISBN);
                                bookRepository.deleteBook(deleteISBN);
                                System.out.println("The book has been removed");
                                break;
                            default:
                                System.out.println("The book has not been deleted!");
                                break;
                        }
                    } else {
                        System.out.println("Book with this ISBN is not exists!");
                    }
                } else {
                    System.out.println("Book repository is empty!");
                }
                break;
            default:
                System.out.println("Enter a correct value!");
                break;
        }

    }

    public void exit() throws IOException, NullPointerException {
        System.out.println("Are you sure to close the program? 1 - YES, other value - NO.");
        String chooseExit = loadCharactersString();

        switch (chooseExit) {
            case "1":
                System.out.println("The program is closing...");
                System.out.println("Bye!");
                System.exit(0);
                break;
            default:
                System.out.println("The program continues...");
        }
    }

    public String loadCharactersString() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return reader.readLine();
    }

    public ISBN loadCharactersISBN() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ISBN isbn = new ISBN(reader.readLine());
        return isbn;
    }

    public ISBN writeISBN() throws IOException {
        System.out.println("\nEnter the number ISBN of the book.");
        ISBN isbn = loadCharactersISBN();
        return isbn;
    }

    public String writeAuthor() throws IOException {
        System.out.println("Enter the author of the book.");
        String author = loadCharactersString();
        return author;
    }

    public String writeTitle() throws IOException {
        System.out.println("Enter the title of the book.");
        String title = loadCharactersString();
        return title;
    }

    public Integer writeYear() throws IOException {

        System.out.println("Enter the year of the book's release.");
        String year = loadCharactersString();
        Integer yearInt;
        try {
            yearInt = toInt(year);
            if (yearInt < 0) {
                System.out.println("Incorrect year!");
                yearInt = writeYear();
            }
            return yearInt;
        } catch (NullPointerException e) {
            System.out.println("Year is not a number!");
            yearInt = writeYear();
            return yearInt;
        } catch (NumberFormatException e) {
            System.out.println("Enter a correct number!");
            yearInt = writeYear();
            return yearInt;
        }

    }

    public Integer toInt(String integerString) throws NumberFormatException {

        Integer integer = Integer.parseInt(integerString);
        return integer;
    }

    public void readFile() throws IOException {

        File file = new File("bookRepository.txt");

        if (!file.exists()) {
            file.createNewFile();
        }

        reader = new BufferedReader(new FileReader(file));
        String line = reader.readLine();

        if (line == null) {
            System.out.println("Book repository is empty!");
        } else {
            while (line != null) {
                String[] parts = line.split("\\|");
                String part0 = parts[0];
                String part1 = parts[1];
                String part2 = parts[2];
                Integer part2Int = toInt(part2);
                String part3 = parts[3];
                ISBN part3ISBN = new ISBN(part3);

                Book book = new Book(part0, part1, part2Int, part3ISBN);

                bookRepository.putToBookRepositoryMap(part3ISBN, book);
                repositoryReservation.putInRepositoryReservationMap(part3ISBN);
                line = reader.readLine();
            }
        }
        reader.close();
    }
}
