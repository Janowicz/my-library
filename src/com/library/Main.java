package com.library;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        MyLibrary myLibrary = new MyLibrary();
        myLibrary.run();
    }
}
