package com.library;

public class Book {

    private String title;
    private String author;
    private Integer year;
    private ISBN isbn;

    public Book(String title, String author, Integer year, ISBN isbn) {
        this.isbn = isbn;
        this.title = title;
        this.author = author;
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public Integer getYear() {
        return year;
    }

    public ISBN getIsbn() {
        return isbn;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Title: ").append(title);
        sb.append(", author: ").append(author);
        sb.append(", year: ").append(year);
        sb.append(", ISBN: ").append(isbn);
        return sb.toString();
    }
}
