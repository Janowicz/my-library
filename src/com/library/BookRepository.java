package com.library;

import java.util.*;

public class BookRepository {

    private Map<ISBN, Book> bookRepositoryMap = new HashMap<>();

    public Collection<Book> getAllBooks() {

        Collection<Book> list = bookRepositoryMap.values();

        return list;
    }

    public boolean ifBookContainsISBN(ISBN isbn) {

        return bookRepositoryMap.containsKey(isbn);
    }

    public void deleteBook(ISBN isbn) {

        Optional.ofNullable(bookRepositoryMap.remove(isbn));
    }

    public Optional<Book> returnBookFromISBN(ISBN isbn) {

        return Optional.ofNullable(bookRepositoryMap.get(isbn));
    }

    public void putToBookRepositoryMap(ISBN isbn, Book book) {

        bookRepositoryMap.put(isbn, book);
    }

    public boolean checkIfEmptyRepository() {

        return bookRepositoryMap.isEmpty();
    }
}
